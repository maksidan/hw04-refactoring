package storage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import shop.Item;
import shop.StandardItem;
import storage.ItemStock;

import static org.junit.jupiter.api.Assertions.*;

public class TestItemStock {
    @Test
    public void testConstructorSuccess() {
        final Item item = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final ItemStock itemStock = new ItemStock(item);

        assertEquals(itemStock.getItem(), item);
        assertEquals(itemStock.getCount(), 0);
    }

    @Test
    public void testConstructorNull() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new ItemStock(null));
    }

    private static int[] successfulCountIncreaseTestsData() {
        return new int[] { 1, 45};
    }

    private static int[] failingCountIncreaseTestsData() {
        return new int[] { 0, -90 };
    }

    private static int[] successfulCountDecreaseTestsData() {
        return new int[] { 1, 6, 10 };
    }

    private static int[] negativeCountDecreaseTestsData() {
        return new int[] { 0, -10 };
    }

    private static int[] tooBigCountDecreaseTestsData() {
        return new int[] { 11, 100 };
    }

    @ParameterizedTest
    @MethodSource("successfulCountIncreaseTestsData")
    public void testIncreaseSuccess(int x) {
        final Item item = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final ItemStock stock = new ItemStock(item);
        final int initCount = stock.getCount();

        stock.increaseItemCount(x);
        assertEquals(stock.getCount(), initCount + x);
    }

    @ParameterizedTest
    @MethodSource("failingCountIncreaseTestsData")
    public void testIncreaseFail(int x) {
        final Item item = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final ItemStock stock = new ItemStock(item);

        Throwable exception = assertThrows(IllegalArgumentException.class, () -> stock.increaseItemCount(x));
        assertEquals(exception.getMessage(), "Argument must be greater than 0.");
    }

    @ParameterizedTest
    @MethodSource("successfulCountDecreaseTestsData")
    public void testDecreaseSuccess(int x) {
        final Item item = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final ItemStock stock = new ItemStock(item);
        stock.increaseItemCount(10);
        int initCount = stock.getCount();

        stock.decreaseItemCount(x);
        assertEquals(stock.getCount(), initCount - x);
    }

    @ParameterizedTest
    @MethodSource("negativeCountDecreaseTestsData")
    public void testDeceaseNegative(int x) {
        final Item item = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final ItemStock stock = new ItemStock(item);
        stock.increaseItemCount(10);

        Throwable exception = assertThrows(IllegalArgumentException.class, () -> stock.decreaseItemCount(x));
        assertEquals(exception.getMessage(), "Argument must be greater than 0.");
    }

    @ParameterizedTest
    @MethodSource("tooBigCountDecreaseTestsData")
    public void testDeceaseTooBigArgument(int x) {
        final Item item = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final ItemStock stock = new ItemStock(item);
        stock.increaseItemCount(10);

        Throwable exception = assertThrows(IllegalArgumentException.class, () -> stock.decreaseItemCount(x));
        assertEquals(exception.getMessage(), "Argument must be less than count.");
    }
}
