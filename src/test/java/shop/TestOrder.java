package shop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TestOrder {
    @Test
    public void testFirstConstructorSuccess() {
        final ArrayList<Item> initialCartContents = new ArrayList<Item>();
        initialCartContents.add(new StandardItem(100, "Banana", 30.5f, "Fruits", 10));
        initialCartContents.add(new StandardItem(167, "Toilet paper", 100f, "Toilet", 21));
        initialCartContents.add(new StandardItem(202, "Chair", 1000.2f, "Furniture", 100));

        final ShoppingCart initialCart = new ShoppingCart(initialCartContents);
        final String initialCustomerName = "John";
        final String initialAddress = "Times Square, 328";
        final int initialState = 1;
        final Order resultOrder = new Order(initialCart, initialCustomerName, initialAddress, initialState);

        assertEquals(resultOrder.getItems(), initialCart.getCartItems());
        assertEquals(resultOrder.getCustomerName(), initialCustomerName);
        assertEquals(resultOrder.getCustomerAddress(), initialAddress);
        assertEquals(resultOrder.getState(), initialState);
    }

    @Test
    public void testFirstConstructorNull() {
        final String initialCustomerName = "Richard";
        final String initialAddress = "Golden gate, 55";
        final int initialState = 2;

        assertThrows(IllegalArgumentException.class,
                () -> new Order(null, initialCustomerName, initialAddress, initialState));
    }

    @Test
    public void testSecondConstructorSuccess() {
        final ArrayList<Item> initialCartContents = new ArrayList<Item>();
        initialCartContents.add(new StandardItem(100, "Banana", 30.5f, "Fruits", 10));
        initialCartContents.add(new StandardItem(167, "Toilet paper", 100f, "Toilet", 21));
        initialCartContents.add(new StandardItem(202, "Chair", 1000.2f, "Furniture", 100));

        final ShoppingCart initialCart = new ShoppingCart(initialCartContents);
        final String initialCustomerName = "John";
        final String initialAddress = "Times Square, 328";
        final Order resultOrder = new Order(initialCart, initialCustomerName, initialAddress);

        assertEquals(resultOrder.getItems(), initialCart.getCartItems());
        assertEquals(resultOrder.getCustomerName(), initialCustomerName);
        assertEquals(resultOrder.getCustomerAddress(), initialAddress);
    }

    @Test
    public void testSecondConstructorNull() {
        final String initialCustomerName = "Richard";
        final String initialAddress = "Golden gate, 55";

        assertThrows(IllegalArgumentException.class,
                () -> new Order(null, initialCustomerName, initialAddress));
    }
}
