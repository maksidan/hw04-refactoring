package shop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

public class TestStandardItem {
    @Test
    public void testConstructor() {
        final int testID = 100;
        final String testName = "Banana";
        final float testPrice = 35.5f;
        final String testCategory = "Fruits";
        final int testLoyaltyPoints = 10;

        StandardItem resultItem = new StandardItem(testID, testName, testPrice, testCategory, testLoyaltyPoints);
        assertEquals(resultItem.getID(), testID);
        assertEquals(resultItem.getName(), testName);
        assertEquals(resultItem.getPrice(), testPrice);
        assertEquals(resultItem.getCategory(), testCategory);
        assertEquals(resultItem.getLoyaltyPoints(), testLoyaltyPoints);
    }

    @Test
    public void testCopySuccess() {
        final StandardItem initialItem = new StandardItem(167, "Toilet paper", 100f, "Toilet", 21);
        final StandardItem copyItem = initialItem.copy();

        assertEquals(copyItem.getID(), initialItem.getID());
        assertEquals(copyItem.getName(), initialItem.getName());
        assertEquals(copyItem.getPrice(), initialItem.getPrice());
        assertEquals(copyItem.getCategory(), initialItem.getCategory());
        assertEquals(copyItem.getLoyaltyPoints(), initialItem.getLoyaltyPoints());
    }

    // Don't know how much actual sense this gives, but in case it does.
    @Test
    public void testCopyFailure() {
        final StandardItem initialItem = new StandardItem(167, "Toilet paper", 100f, "Toilet", 21);
        final StandardItem otherItem = new StandardItem(344, "Bubble gum", 15f, "Sweets", 5);
        final StandardItem copyItem = initialItem.copy();

        assertNotEquals(copyItem.getID(), otherItem.getID());
        assertNotEquals(copyItem.getName(), otherItem.getName());
        assertNotEquals(copyItem.getPrice(), otherItem.getPrice());
        assertNotEquals(copyItem.getCategory(), otherItem.getCategory());
        assertNotEquals(copyItem.getLoyaltyPoints(), otherItem.getLoyaltyPoints());
    }

    private static StandardItem[] testItems() {
        return new StandardItem[] {
                new StandardItem(202, "Chair", 1000.2f, "Furniture", 100) };
    }

    @ParameterizedTest
    @MethodSource("testItems")
    public void testEqualsSuccess(Object object) {
        final StandardItem initialItem = new StandardItem(202, "Chair", 1000.2f, "Furniture", 100);
        assertTrue(initialItem.equals(object));
    }

    @ParameterizedTest
    @MethodSource("testItems")
    public void testEqualsFailure(Object object) {
        final StandardItem initialItem = new StandardItem(333, "Dog", 5000f, "Animals", 300);
        assertFalse(initialItem.equals(object));
    }
}
