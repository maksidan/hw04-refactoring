package shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import storage.NoItemInStorage;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.junit.jupiter.api.Assertions.*;

public class TestEShopController {
    private final EShopController shopController = new EShopController();
    private int[] itemCount;
    private Item[] storageItems;

    @BeforeEach
    public void init() {
        shopController.startEShop();

        itemCount = new int[] {10, 10, 4, 5, 10, 2};

        storageItems = new Item[] {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        for (int i = 0; i < storageItems.length; i++) {
            shopController.getStorage().insertItems(storageItems[i], itemCount[i]);
        }
    }

    @Test
    public void testPurchaseEmptyCart() {
        ShoppingCart emptyCart = new ShoppingCart();
        String resultText = "";
        try {
            resultText = tapSystemOut(() ->
                    shopController.purchaseShoppingCart(emptyCart, "Preston Garvey", "Wasteland"));
        } catch (Exception e) {
            fail("Unexpected NoItemInStorage exception");
        }

        assertEquals("Error: shopping cart is empty\n", resultText);
    }

    @Test
    public void testPurchaseNonExistingItem() {
        ShoppingCart cart = new ShoppingCart();
        Item nonExistingItem = new StandardItem(100, "Wall", 1000, "Furniture", 10);
        cart.addItem(nonExistingItem);

        assertThrows(NoItemInStorage.class, () ->
                shopController.purchaseShoppingCart(cart, "Tony Hawk", "Skate park"));
    }

    @Test
    public void testPurchaseOutOfStockItem() {
        ShoppingCart cart = new ShoppingCart();
        shopController.getStorage().setItemCount(storageItems[4].getID(), 0);

        cart.addItem(storageItems[4]);
        assertThrows(NoItemInStorage.class, () ->
                shopController.purchaseShoppingCart(cart, "Solid Snake", "Secret Mission"));
    }

    @Test
    public void testPurchaseItemSuccess() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(storageItems[3]);
        cart.addItem(storageItems[5]);

        try {
            shopController.purchaseShoppingCart(cart, "Lara Croft", "Jungle Tomb");
        } catch (Exception e) {
            fail("Unexpected NoItemInStorage exception.");
        }

        assertEquals(1, shopController.getArchive().getHowManyTimesHasBeenItemSold(storageItems[3]));
        assertEquals(1, shopController.getArchive().getHowManyTimesHasBeenItemSold(storageItems[5]));

        assertEquals(shopController.getStorage().getItemCount(storageItems[3].getID()), itemCount[3] - 1);
        assertEquals(shopController.getStorage().getItemCount(storageItems[5].getID()), itemCount[5] - 1);
    }

    @Test
    public void testRemoveItemFromEmptyCart() {
        ShoppingCart cart = new ShoppingCart();
        assertThrows(IllegalArgumentException.class, () -> cart.removeItem(storageItems[2].getID()));
    }

    @Test
    public void testRemoveItemNotInCart() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(storageItems[0]);
        assertThrows(IllegalArgumentException.class, () -> cart.removeItem(storageItems[3].getID()));
    }

    @Test
    public void testRemoveItemFromCartSuccess() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(storageItems[5]);
        cart.removeItem(storageItems[5].getID());
        assertEquals(false, cart.containsItem(storageItems[5]));
    }
}
