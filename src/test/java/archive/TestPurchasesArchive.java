package archive;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.util.ArrayList;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestPurchasesArchive {
    @Test
    public void testPrintItemPurchaseStatistics() {
        final ArrayList<Item> items = new ArrayList<>();
        final Item firstItem = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final Item secondItem = new StandardItem(167, "Toilet paper", 100f, "Toilet", 21);
        final Item thirdItem = new StandardItem(344, "Bubble gum", 15f, "Sweets", 5);
        items.add(firstItem);
        items.add(secondItem);
        items.add(thirdItem);

        final ShoppingCart cart = new ShoppingCart(items);
        final Order order = new Order(cart, "Boris", "Johnson");

        final PurchasesArchive archive = new PurchasesArchive();
        archive.putOrderToPurchasesArchive(order);
        archive.putOrderToPurchasesArchive(order);
        archive.putOrderToPurchasesArchive(order);

        String resultText = "";
        try {
            resultText = tapSystemOut(archive::printItemPurchaseStatistics);
        } catch (Exception e) {
            System.out.println("Failed to get output. Make sure redirection implemented right.");
        }

        System.out.println(resultText.trim());

        String expectedOutput = "ITEM PURCHASE STATISTICS:\n" +
                "ITEM  Item   ID 167   NAME Toilet paper   CATEGORY Toilet   PRICE 100.0   LOYALTY POINTS 21   HAS BEEN SOLD 3 TIMES\n" +
                "ITEM  Item   ID 344   NAME Bubble gum   CATEGORY Sweets   PRICE 15.0   LOYALTY POINTS 5   HAS BEEN SOLD 3 TIMES\n" +
                "ITEM  Item   ID 11   NAME T-Shirt   CATEGORY Clothes   PRICE 300.0   LOYALTY POINTS 30   HAS BEEN SOLD 3 TIMES";

        assertEquals(expectedOutput, resultText.trim());
    }

    private static Item[] testHowManyTimesHasBeenItemSoldData() {
        return new Item[] {
                new StandardItem(11, "T-Shirt", 300f, "Clothes", 30),
                new StandardItem(167, "Toilet paper", 100f, "Toilet", 21),
                null
        };
    }

    @ParameterizedTest
    @MethodSource("testHowManyTimesHasBeenItemSoldData")
    public void testGetHowManyTimesHasBeenItemSold(Item item) {
        final PurchasesArchive archive = new PurchasesArchive();

        // Check it fails if item is null
        if (item == null) {
            assertThrows(IllegalArgumentException.class, () -> archive.getHowManyTimesHasBeenItemSold(null));
            return;
        }

        final ArrayList<Item> items = new ArrayList<>();
        final boolean shouldBeInArchive = (item.getName().equals(testHowManyTimesHasBeenItemSoldData()[0].getName()));
        if (shouldBeInArchive) {
            items.add(item);
        }

        final ShoppingCart cart = new ShoppingCart(items);
        final Order order = new Order(cart, "Emmanuel", "Macron");

        archive.putOrderToPurchasesArchive(order);
        archive.putOrderToPurchasesArchive(order);
        archive.putOrderToPurchasesArchive(order);
        archive.putOrderToPurchasesArchive(order);

        if (shouldBeInArchive) {
            assertEquals(archive.getHowManyTimesHasBeenItemSold(item), 4);
        } else {
            assertEquals(archive.getHowManyTimesHasBeenItemSold(item), 0);
        }
    }

    @Test
    public void testPutOrderToPurchaseArchive() {
        final PurchasesArchive archive = new PurchasesArchive();

        // Test it fails if Order is null
        assertThrows(IllegalArgumentException.class, () -> archive.putOrderToPurchasesArchive(null));

        final Item firstItem = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final Item secondItem = new StandardItem(344, "Bubble gum", 15f, "Sweets", 5);
        final Item thirdItem = new StandardItem(167, "Toilet paper", 100f, "Toilet", 21);

        final ArrayList<Item> items = new ArrayList<>();
        items.add(firstItem);
        items.add(firstItem);
        items.add(secondItem);
        final ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart, "Joe", "Biden");

        archive.putOrderToPurchasesArchive(order);

        assertEquals(archive.getHowManyTimesHasBeenItemSold(firstItem), 2);
        assertEquals(archive.getHowManyTimesHasBeenItemSold(secondItem), 1);
        assertEquals(archive.getHowManyTimesHasBeenItemSold(thirdItem), 0);
    }

    @Test
    public void mockTestOrderArchive() {
        final PurchasesArchive mockArchive = mock(PurchasesArchive.class);

        final Item firstItem = new StandardItem(11, "T-Shirt", 300f, "Clothes", 30);
        final Item secondItem = new StandardItem(344, "Bubble gum", 15f, "Sweets", 5);

        final ArrayList<Item> items = new ArrayList<>();
        items.add(firstItem);
        items.add(firstItem);
        items.add(secondItem);
        final ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart, "Joe", "Biden");

        mockArchive.putOrderToPurchasesArchive(order);
        verify(mockArchive).putOrderToPurchasesArchive(order);

        mockArchive.getHowManyTimesHasBeenItemSold(firstItem);
        verify(mockArchive).getHowManyTimesHasBeenItemSold(firstItem);
    }

    @Test
    public void mockTestItemPurchaseArchiveEntry() {
        final ItemPurchaseArchiveEntry mockArchiveEntry = mock(ItemPurchaseArchiveEntry.class);

        mockArchiveEntry.increaseCountHowManyTimesHasBeenSold(2);
        verify(mockArchiveEntry).increaseCountHowManyTimesHasBeenSold(2);

        mockArchiveEntry.getCountHowManyTimesHasBeenSold();
        verify(mockArchiveEntry).getCountHowManyTimesHasBeenSold();

        mockArchiveEntry.getRefItem();
        verify(mockArchiveEntry).getRefItem();
    }

    @Test
    public void testItemPurchaseArchiveEntryConstructorSuccess() {
        final Item expectedItem = new StandardItem(167, "Toilet paper", 100f, "Toilet", 21);
        final ItemPurchaseArchiveEntry archiveEntry = new ItemPurchaseArchiveEntry(expectedItem);

        final Item resultItem = archiveEntry.getRefItem();
        assertEquals(expectedItem, resultItem);

        final int resultCount = archiveEntry.getCountHowManyTimesHasBeenSold();
        assertEquals(1, resultCount);
    }

    @Test
    public void testItemPurchaseArchiveEntryConstructorFail() {
        assertThrows(IllegalArgumentException.class,
                () -> new ItemPurchaseArchiveEntry(null));
    }

    @Test
    public void testPrintln() {
        String testingText = "This is a testing text!";
        String resultText = "";
        try {
            resultText = tapSystemOut(() -> System.out.println(testingText));
        } catch (Exception e) {
            System.out.println("Failed to get output. Make sure redirection implemented right.");
        }

        assertEquals(testingText + "\n", resultText);
    }
}
