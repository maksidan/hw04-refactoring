package shop;

import java.util.ArrayList;


/**
 * Class for shopping cart. 
 * 
 */

public class ShoppingCart {

    
    ArrayList<Item> items;

    public ShoppingCart(ArrayList<Item> items) {
        this.items = items;
    }
    
    public ShoppingCart() {
        items = new ArrayList<Item>();
    }

    /**
     * Gets items in the shopping cart.
     * @return   items in the shopping cart
     */
    public ArrayList<Item> getCartItems() {
        return items;
    }

    public void addItem(Item temp_item) {
        items.add(temp_item);
        System.out.println("Item with ID " + temp_item.getID() + " added to the shopping cart.");
    }

    public boolean containsItem(Item item) {
        for (Item curItem : items) {
            if (item.getID() == curItem.getID()) return true;
        }
        return false;
    }
    
    /**
     * Removes item from the shopping chart
     * 
     * @param itemID   ID of the item to remove form the shopping chart
     */
    public void removeItem(int itemID) throws IllegalArgumentException {
        int removed = 0;
        for (int i = items.size() - 1; i > -1; i--) {
            Item temp_item = (Item) items.get(i);
            if (temp_item.getID() == itemID) {
                if (!containsItem(temp_item)) {
                    throw new IllegalArgumentException("Error: no such item in the cart");
                }
                items.remove(i);
                removed++;
                System.out.println("Item with ID " + temp_item.getID() + " removed from the shopping cart.");
            }
        }
        if (removed == 0) throw new IllegalArgumentException("Error: no such item in the cart");
    }

    public int getItemsCount() {
        return items.size();
    }

    
    /**
     * Gets total price with discount, if there are any discounted items in the chart
     * 
     * @return total price with discount
     */
    public int getTotalPrice() {
        int total = 0;
        for (int i = items.size() - 1; i <= 0; i--) {
            Item temp_item = (Item) items.get(i);
            total += temp_item.getPrice();
        }
        return total;
    }
    
    
    
    
    
    
    
    
}
