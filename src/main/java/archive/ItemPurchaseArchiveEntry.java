package archive;

import org.jetbrains.annotations.NotNull;
import shop.Item;

class ItemPurchaseArchiveEntry {
    private Item refItem;
    private int soldCount;
    
    ItemPurchaseArchiveEntry(@NotNull Item refItem) throws IllegalArgumentException {
        if (refItem == null) throw new IllegalArgumentException("Item can't be null.");
        this.refItem = refItem;
        soldCount = 1;
    }
    
    public void increaseCountHowManyTimesHasBeenSold(int x) {
        soldCount += x;
    }
    
    public int getCountHowManyTimesHasBeenSold() {
        return soldCount;
    }
    
    public Item getRefItem() {
        return refItem;
    }
    
    @Override
    public String toString() {
        return "ITEM  " + refItem.toString() + "   HAS BEEN SOLD "+soldCount+" TIMES";
    }
}
