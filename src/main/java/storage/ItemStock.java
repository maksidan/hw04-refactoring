package storage;

import org.jetbrains.annotations.NotNull;
import shop.*;


/**
 * Auxiliary class for item storage
 */
public class ItemStock {
    private Item refItem;
    private int count;
    
    public ItemStock(@NotNull Item refItem) throws IllegalArgumentException {
        this.refItem = refItem;
        count = 0;
    }
    
    @Override
    public String toString() {
        return "STOCK OF ITEM:  "+refItem.toString()+"    PIECES IN STORAGE: "+count;
    }
    
    public void increaseItemCount(int x) throws IllegalArgumentException {
        if (x < 1) throw new IllegalArgumentException("Argument must be greater than 0.");
        count += x;
    }
    
    public void decreaseItemCount(int x) {
        if (x < 1) throw new IllegalArgumentException("Argument must be greater than 0.");
        if (x > count) throw new IllegalArgumentException("Argument must be less than count.");
        count -= x;
    }
    
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    public Item getItem() {
        return refItem;
    }
}